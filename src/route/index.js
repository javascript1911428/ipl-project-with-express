const express = require("express");
const app = express();
const port = 3000;
const path = require("path");

const problem1 = require("../server/1-matches-per-year.cjs");
const problem2 = require("../server/2-matches-won-per-team-per-year.cjs");
const problem3 = require("../server/3-extra-runs-conceeded-per-team-in-2016.cjs");
const problem4 = require("../server/4-top-ten-economical-bowlers-in-2015.cjs");
const problem5 = require("../server/5-no-of-times-toss-and-match-won.cjs");
const problem6 = require("../server/6-highest-no-of-player-of-the-match.cjs");
const problem7 = require("../server/7-strikerate-of-batsmen-each-season.cjs");
const problem8 = require("../server/8-highest-dismissal.cjs");
const problem9 = require("../server/9-economical-bowlers-in-superover.cjs");


app.get("/problem1", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem1);
});

app.get("/problem2", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem2);
});

app.get("/problem3", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem3);
});

app.get("/problem4", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem4);
});

app.get("/problem5", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem5);
});

app.get("/problem6", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem6);
});

app.get("/problem7", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem7);
});

app.get("/problem8", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem8);
});

app.get("/problem9", (req, res) => {
    res.setHeader("Content-Type", "text/json");
    res.send(problem9);
});

app.listen(port, (err) => {
    if (err) {
        console.log("Error:", err)
    }
    console.log(`listening to ${port}`)
});