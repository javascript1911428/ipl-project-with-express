const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvFilePath = path.join(__dirname, "..", "data/matches.csv");

const matches = fs.readFileSync(csvFilePath, "utf-8")
const parsedData = papa.parse(matches, {
    header: true,
})

matchesOfAllYears = parsedData.data;

function getMatchesPerYear(matchesOfAllYears) {
    let matchesPerYear = matchesOfAllYears.reduce((accumulator, match) => {
        let season = parseInt(match["season"]);
        if (accumulator[season] === undefined) {
            accumulator[season] = 1;
        } else {
            accumulator[season] += 1;
        }
        return accumulator
    }, {});
    return matchesPerYear;
}

let result = getMatchesPerYear(matchesOfAllYears);
let jsonData = JSON.stringify(result);

module.exports = jsonData;

