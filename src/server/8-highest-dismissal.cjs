const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvDeliveriesFilePath = path.join(__dirname, "..", "data/deliveries.csv");

const deliveries = fs.readFileSync(csvDeliveriesFilePath, "utf-8")
const parsedDeliveryData = papa.parse(deliveries, {
    header: true,
})

let deliveriesData = parsedDeliveryData.data;

function findHighestDismissals(deliveriesData) {
    const dismissalCount = deliveriesData.reduce((accumulator, dismissal) => {
        const batsman = dismissal['player_dismissed'];
        const bowler = dismissal['bowler'];
        const dismissalKind = dismissal['dismissal_kind']

        if (dismissalKind !== "run out" && batsman !== "") {
            if (!accumulator[batsman]) {
                accumulator[batsman] = {};
            }

            if (!accumulator[batsman][bowler]) {
                accumulator[batsman][bowler] = 1;
            } else {
                accumulator[batsman][bowler]++;
            }
        }
        return accumulator
    }, {});

    const highestDismissals = Object.entries(dismissalCount).reduce((accumulator, [batsman, bowlerStats]) => {
        const bowlerWithMaxDismissals = Object.entries(bowlerStats).reduce((maxDismissals, [bowler, count]) => {
            if (count > maxDismissals.count) {
                return { bowler, count };
            } else {
                return maxDismissals
            }
        }, { bowler: '', count: 0 });
        accumulator[batsman] = { bowler: bowlerWithMaxDismissals.bowler, count: bowlerWithMaxDismissals.count };
        return accumulator
    }, {});

    const sortedDismissals = Object.entries(highestDismissals).sort((a, b) => b[1].count - a[1].count);
    return sortedDismissals[0];
}

let result = findHighestDismissals(deliveriesData)

const jsonData = JSON.stringify(result);

module.exports = jsonData;