const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvMatchesFilePath = path.join(__dirname, "..", "data/matches.csv");
const csvDeliveriesFilePath = path.join(__dirname, "..", "data/deliveries.csv");

const matches = fs.readFileSync(csvMatchesFilePath, "utf-8")
const parsedMatchData = papa.parse(matches, {
    header: true,
})

let matchesData = parsedMatchData.data;

const deliveries = fs.readFileSync(csvDeliveriesFilePath, "utf-8")
const parsedDeliveryData = papa.parse(deliveries, {
    header: true,
})

let deliveriesData = parsedDeliveryData.data;

function strikerateOfBatsmenEverySeason(matchesData, deliveriesData) {
    const distinctYears = matchesData.reduce((accumulator, match) => {
        let season = match["season"];
        if(!accumulator.includes(season)) {
            accumulator.push(season);
        }
        return accumulator;
    }, []);

    const matchIdForSeason = distinctYears.reduce((accumulator, year) => {
        const yearList = matchesData.filter((match) => match["season"] === year).map((match) => match["id"]);
        accumulator[year] = yearList;
        return accumulator;
    }, {});

    const toatlRunsByBatsmanSeason = distinctYears.reduce((accumulator, year) => {
        accumulator[year] = deliveriesData.reduce((result, delivery) => {
            if(matchIdForSeason[year].includes(delivery['match_id'])) {
                const batsman = delivery['batsman'];
                const runs = parseInt(delivery['batsman_runs']);
                const wideRuns = delivery['wide_runs'];
                const noballRuns = delivery['noball_runs'];

                if(!result[batsman]) {
                    result[batsman] = {runs:0, balls:0};
                }

                result[batsman].runs += runs;

                if (wideRuns === '0' && noballRuns === '0') {
                    result[batsman].balls++;
                }
            }
            return result;
        }, {});
        return accumulator;
    }, {});

    const strikeRateByBatsmanSeason = distinctYears.reduce((accumulator, year) => {
        accumulator[year] = {};
        Object.entries(toatlRunsByBatsmanSeason[year]).map(([batsman, stats]) => {
            const {runs, balls} = stats;
            accumulator[year][batsman] = ((runs / balls) * 100).toFixed(2);
        });
        return accumulator;
    }, {});
    return strikeRateByBatsmanSeason;
}

let result = strikerateOfBatsmenEverySeason(matchesData, deliveriesData)

let jsonData = JSON.stringify(result);

module.exports = jsonData;