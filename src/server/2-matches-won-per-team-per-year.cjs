const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvFilePath = path.join(__dirname, "..", "data/matches.csv");

const matches = fs.readFileSync(csvFilePath, "utf-8")
const parsedData = papa.parse(matches, {
    header: true,
})

matchesOfAllYears = parsedData.data;

function getMatchesWonPerTeamPerYear(matchesOfAllYears) {
    const matchesWonPerTeamPerYear = matchesOfAllYears.reduce((accumulator, match) => {
        const season = match["season"];
        const winner = match["winner"];

        if(!accumulator[season]) {
            accumulator[season] = {};
        }

        if(!accumulator[season][winner]) {
            accumulator[season][winner] = 1;
        } else {
            accumulator[season][winner] +=1;
        }

        return accumulator;
    }, {})
    return matchesWonPerTeamPerYear;
}

let result = getMatchesWonPerTeamPerYear(matchesOfAllYears);
const jsonData = JSON.stringify(result);

module.exports = jsonData;