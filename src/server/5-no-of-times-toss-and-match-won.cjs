const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvFilePath = path.join(__dirname, "..", "data/matches.csv");

const matches = fs.readFileSync(csvFilePath, "utf-8")
const parsedData = papa.parse(matches, {
    header: true,
})

let matchesData = parsedData.data;

const matchEntries = matchesData.filter((match) => match['winner'] === match['toss_winner']);

const noOfTimesMatchAndTossWon = matchEntries.reduce((accumulator, entry) => {
    if(!accumulator[entry['toss_winner']]) {
        accumulator[entry['toss_winner']] = 1;
    
    } else {
        accumulator[entry['toss_winner']] += 1;
    }
    return accumulator;
}, {})


// Convert to JSON
const jsonData = JSON.stringify(noOfTimesMatchAndTossWon);

module.exports = jsonData;