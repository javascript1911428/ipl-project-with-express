const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvFilePath = path.join(__dirname, "..", "data/matches.csv");

const matches = fs.readFileSync(csvFilePath, "utf-8")
const parsedData = papa.parse(matches, {
    header: true,
})

let matchesData = parsedData.data;

function highestNoOfPlayerOfTheMatch(matchesData) {
    const distinctYears = matchesData.reduce((accumulator, match) => {
        let season = match["season"];
        if(!accumulator.includes(season)) {
            accumulator.push(season);
        }
        return accumulator;
    }, []);
    const highestManOfTheMatch = distinctYears.reduce((accumulator, season) => {
        const manOfTheMatch = matchesData.reduce((result, match) => {
          if (match['season'] === season) {
            const playerOfMatch = match['player_of_match'];
            result[playerOfMatch] = (result[playerOfMatch] || 0) + 1;
          }
          return result
        }, {});
        const sortedData = Object.fromEntries(
          Object.entries(manOfTheMatch).sort((a, b) => b[1] - a[1])
  
        );
  
        accumulator[season] = Object.keys(sortedData)[0];
        return accumulator
      }, {});
  
      return highestManOfTheMatch;
}
  
let result = highestNoOfPlayerOfTheMatch(matchesData);

let jsonData = JSON.stringify(result);

module.exports = jsonData;