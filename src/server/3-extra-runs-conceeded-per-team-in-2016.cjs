const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvMatchesFilePath = path.join(__dirname, "..", "data/matches.csv");
const csvDeliveriesFilePath = path.join(__dirname, "..", "data/deliveries.csv");

const matches = fs.readFileSync(csvMatchesFilePath, "utf-8")
const parsedMatchData = papa.parse(matches, {
    header: true,
})

let matchesOfAllYears = parsedMatchData.data;

const deliveries = fs.readFileSync(csvDeliveriesFilePath, "utf-8")
const parsedDeliveryData = papa.parse(deliveries, {
    header: true,
})

let deliveriesOfAllYears = parsedDeliveryData.data;

function extraRunsConcededPerTeamIn2016(matchesOfAllYears, deliveriesOfAllYears) {
    const matchesIn2016 = matchesOfAllYears.filter((match) => match["season"] === "2016");
    const idList = matchesIn2016.map((match) => match["id"]).sort();
    const minimumId = idList[0];
    const maximumId = idList[idList.length - 1];

    const extraRunsConcededPerTeam = deliveriesOfAllYears.reduce((accumulator, delivery) => {
        const matchId = parseInt(delivery["match_id"]);
        if(matchId >= parseInt(minimumId) && matchId <= parseInt(maximumId)) {
            const bowlingTeam = delivery["bowling_team"];
            const extraRuns = parseInt(delivery["extra_runs"]);
            if(!accumulator[bowlingTeam]) {
                accumulator[bowlingTeam] = extraRuns;
            } else {
                accumulator[bowlingTeam] += extraRuns;
            }
        }
        return accumulator;
    }, {})
    return extraRunsConcededPerTeam;
}

let result = extraRunsConcededPerTeamIn2016(matchesOfAllYears, deliveriesOfAllYears)
const jsonData = JSON.stringify(result);

module.exports = jsonData;