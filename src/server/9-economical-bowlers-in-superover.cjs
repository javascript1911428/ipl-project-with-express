const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvDeliveriesFilePath = path.join(__dirname, "..", "data/deliveries.csv");

const deliveries = fs.readFileSync(csvDeliveriesFilePath, "utf-8")
const parsedDeliveryData = papa.parse(deliveries, {
    header: true,
})

let deliveriesData = parsedDeliveryData.data;

function economicBowlerInSuperOver(deliveriesData) {
    const superOverBowlers = deliveriesData
        .filter((match) => match['is_super_over'] !== "0" && match['bowler'] !== undefined)
        .reduce((acc, match) => {
            const bowler = match['bowler'];
            if (!acc[bowler]) {
                acc[bowler] = 0;
            }
            return acc;
        }, {});

    const superOverBowlerRuns = Object.keys(superOverBowlers).reduce((acc, bowler) => {
        const ballCountAndTotalScore = deliveriesData
            .filter((match) => match['bowler'] === bowler && match['is_super_over'] === "1")
            .reduce((result, match) => {
                const wideRuns = parseInt(match['wide_runs']);
                const noballRuns = parseInt(match['noball_runs']);
                if (wideRuns === 0 && noballRuns === 0) {
                    result.ballCount += 1;
                }
                result.totalScore += parseInt(match['total_runs']) - parseInt(match['legbye_runs']) - parseInt(match['penalty_runs']) - parseInt(match['bye_runs']);
                return result;
            }, { ballCount: 0, totalScore: 0 });

        acc[bowler] = [ballCountAndTotalScore.totalScore, ballCountAndTotalScore.ballCount];
        return acc;
    }, {});

    const economyOfSuperOver = Object.entries(superOverBowlerRuns).reduce((acc, [superBowler, [totalScore, ballCount]]) => {
        const economy = (totalScore / ballCount) * 6;
        acc[superBowler] = economy;
        return acc;
    }, {});

    const bowlingAverageArray = Object.entries(economyOfSuperOver).sort((a, b) => a[1] - b[1]);

    return bowlingAverageArray[0];

}

let result = economicBowlerInSuperOver(deliveriesData)

const jsonData = JSON.stringify(result);

module.exports = jsonData;