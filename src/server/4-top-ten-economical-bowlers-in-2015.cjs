const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

const csvMatchesFilePath = path.join(__dirname, "..", "data/matches.csv");
const csvDeliveriesFilePath = path.join(__dirname, "..", "data/deliveries.csv");

const matches = fs.readFileSync(csvMatchesFilePath, "utf-8")
const parsedMatchData = papa.parse(matches, {
    header: true,
})

let matchesData = parsedMatchData.data;

const deliveries = fs.readFileSync(csvDeliveriesFilePath, "utf-8")
const parsedDeliveryData = papa.parse(deliveries, {
    header: true,
})

let deliveriesData = parsedDeliveryData.data;

function topEconomyBowlersIn2015(matchesData, deliveriesData) {
    const matchesIn2015 = matchesData.filter((match) => match['season'] === "2015");
    const idList = matchesIn2015.map((match) => match['id']).sort()
    const minimumId = idList[0];
    const maximumId = idList[idList.length - 1];

    const bowlersData = deliveriesData.reduce((accumulator, delivery) => {
        const matchId = parseInt(delivery['match_id']);
        if(matchId >= parseInt(minimumId)  && matchId <= parseInt(maximumId)) {

            const bowler = (delivery['bowler']);
            const totalRuns = delivery['total_runs'];
            const penaltyRuns = delivery['penalty_runs'];
            const byeRuns = delivery['bye_runs'];
            const legByeRuns = delivery['legbye_runs'];
            const wideRuns = delivery['wide_runs']
            const noballRuns = delivery['noball_runs'];

            if (!accumulator[bowler]) {
                accumulator[bowler] = {runs: 0, balls: 0};
            }

            if(wideRuns === "0" && noballRuns === "0") {
                accumulator[bowler]['runs'] += (parseInt(totalRuns) - parseInt(penaltyRuns) -parseInt(byeRuns) - parseInt(legByeRuns));
                accumulator[bowler]['balls']++;
            }
        }   
        return accumulator
        }, {});

    for (const bowler in bowlersData) {
        const runs = bowlersData[bowler]['runs']
        const balls = bowlersData[bowler]['balls']
        bowlersData[bowler].economy = (runs / balls) * 6;
    }

    const sortedBowlers = Object.entries(bowlersData).sort(
        (a, b) => a[1].economy - b[1].economy
    );

    const top10EconomyBowlers = sortedBowlers.slice(0, 10).map((entry) => ({
        bowler: entry[0],
        economy: entry[1].economy,
    }));

    return top10EconomyBowlers;
}

let result = topEconomyBowlersIn2015(matchesData, deliveriesData)

const jsonData = JSON.stringify(result);

module.exports = jsonData;